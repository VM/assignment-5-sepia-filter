#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "image_tools/bmp_format.h"
#include "image_tools/utils.h"
#include "image_tools/image.h"
#include "sepia/sepia.h"

#define profile(what, times, func) 	start = clock();\
									for(size_t i = 0; i < times; i++) func \
									printf("Avg time taken by " #what ": %f ms\n", ((double) (clock() - start)) / CLOCKS_PER_SEC * 1000 / times);

int save_image(struct image img, const char* name, const char* preff);
void run_profiling(struct image img);

int main( int argc, char** argv ) {

    if(argc != 3){
		fprintf(stderr, "Usage: %s <input> <output>.bmp\n", argv[0]);
		return 1;
	}
    const char* input = argv[1];
	const char* output = argv[2];

    FILE* bmp = 0;
    log_error(input, open_file_read(input, &bmp));
    struct image img;
    if(bmp == 0)
    	return 1;

    enum read_status status = from_bmp(bmp, &img);
    fclose(bmp);
    if(status != READ_OK){
    	switch(status){
			case READ_INVALID_SIGNATURE:
				fprintf(stderr, "Invalid signature of file\n");
				break;
			case READ_INVALID_BITS:
				fprintf(stderr, "Invalid bits per pixel, only 24 b/p is supported\n");
				break;
			case READ_INVALID_HEADER:
				fprintf(stderr, "Invalid header\n");
				break;
			case READ_INVALID_PIXELS:
				fprintf(stderr, "Invalid pixels array\n");
				break;
			case READ_ALLOCATION_ERROR:
				fprintf(stderr, "Error allocate memory to load image\n");
				break;
			default:
				fprintf(stderr, "Unknown error\n");
				break;
    	}
		return 1;
    }

    struct image img1 = {0};
    struct image img2 = {0};

    img1 = apply_sepia_c(img);
    img2 = apply_sepia_asm(img);

    free_image(&img);

    return save_image(img1, output, "c") + save_image(img2, output, "asm");
}

void run_profiling(struct image const img){
    clock_t start;
	struct image img1 = {0};

    profile(sepia c, 100, {
    	img1 = apply_sepia_c(img);
    	free_image(&img1);
    })

    profile(sepia asm, 100, {
    	img1 = apply_sepia_asm(img);
    	free_image(&img1);
    })
}

int save_image(struct image img, const char* name, const char* preff){
    FILE* out = 0;
    char* filename = malloc(strlen(name) + strlen(preff) + 1 + 5);

	strcpy(filename, name);
	strcat(filename, "-");
	strcat(filename, preff);
	strcat(filename, ".bmp");

    log_error(filename, open_file_write(filename, &out));
    free(filename);
    if(out == 0){
    	free_image(&img);
    	return 1;
    }
    enum write_status write_status = to_bmp(out, &img);
	fclose(out);
	if(img.data != 0) free_image(&img);
	if(write_status != WRITE_OK){
		fprintf(stderr, "Error writing file\n");
		return 1;
	}
	return 0;
}
