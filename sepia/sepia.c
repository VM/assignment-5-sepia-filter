#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/param.h>

#include "../image_tools/image.h"

#define init_vector(val) (struct vec){val, val, val, val}

typedef struct __attribute__((__packed__)) vec {
	float val1, val2, val3, val4;
} vec;

void apply_sepia(struct pixel *p);
void calc_sepia_colors(struct pixel*, vec*);

struct image apply_sepia_c(struct image const img) {
	struct image sepia_img = { 0 };

	const uint64_t width = get_width(img);
	const uint64_t height = get_height(img);

	copy_image(img, &sepia_img);

	if(sepia_img.data == NULL)
			return (struct image){0};


	for (size_t y = 0; y < height; y++) {
		for (size_t x = 0; x < width; x++) {
			apply_sepia(get_pixel(sepia_img, x, y));
		}
	}
	return sepia_img;
}

// Using SSE vector instructions to speed up filter
struct image apply_sepia_asm(struct image const img) {
	struct image sepia_img = { 0 };
	vec* matrix = malloc(sizeof(vec) * 9);

	copy_image(img, &sepia_img);

	if(sepia_img.data == NULL || matrix == NULL)
			return (struct image){0};

	// blue coefficients
	matrix[0] = init_vector(0.272);
	matrix[1] = init_vector(0.534);
	matrix[2] = init_vector(0.131);

	// green coefficients
	matrix[3] = init_vector(0.349);
	matrix[4] = init_vector(0.686);
	matrix[5] = init_vector(0.168);

	// red coefficients
	matrix[6] = init_vector(0.393);
	matrix[7] = init_vector(0.769);
	matrix[8] = init_vector(0.189);

	const uint64_t width = get_width(img);
	const uint64_t height = get_height(img);

	// Align to 4, to avoid unmapped memory access
	const uint64_t size = width * height - width * height % 4;

	struct pixel *p = get_pixels(sepia_img, -1);
	for (size_t i = 0; i < size; i += 4) {
		calc_sepia_colors(p + i, matrix);
	}

	if(width * height % 4 != 0)// End sepia applying if pixels count is not multiple of 4
		calc_sepia_colors(p + size - 4, matrix);

	free(matrix);

	return sepia_img;
}

void apply_sepia(struct pixel *p) {
	uint8_t g = p->g, b = p->b, r = p->r;
	p->r = MIN((r * 0.393) + (g * 0.769) + (b * 0.189), 255);
	p->g = MIN(((r * 0.349) + (g * 0.686) + (b * 0.168)), 255);
	p->b = MIN(((r * 0.272) + (g * 0.534) + (b * 0.131)), 255);
}

