#include "../image_tools/image.h"

struct image apply_sepia_c(struct image const img);
struct image apply_sepia_asm(struct image const img);
