%define PIX_SIZE 3
%define XMM_SIZE 16

%define MACRO_REG r8
%define MACRO_WORD_REG %[MACRO_REG]w
%define MACRO_BYTE_REG %[MACRO_REG]b

%define BYTE_MAX r9

; min(reg, reg)
%macro min 2
cmp %1, %2
cmova %1, %2
%endmacro

; fill_xmm(struct pixel* pix, int field_offset, register_to_fill)
%macro fill_xmm 3
	movzx MACRO_REG, byte [%1 + PIX_SIZE * 3 + %2]
	sal MACRO_REG, 32
	mov MACRO_BYTE_REG, byte [%1 + PIX_SIZE * 2 + %2]
	movq mm0, MACRO_REG
	cvtpi2ps %3, mm0
	movlhps %3, %3

	movzx MACRO_REG, byte [%1 + PIX_SIZE + %2]
	sal MACRO_REG, 32
	mov MACRO_BYTE_REG, byte [%1 + %2]
	movq mm0, MACRO_REG
	cvtpi2ps %3, mm0
%endmacro

; unfill_xmm(struct pixel* pix, int field_offset, register_to_unfill)
%macro unfill_xmm 3
	cvtps2pi mm0, %3
	movq MACRO_REG, mm0
	min MACRO_WORD_REG, %[BYTE_MAX]w
	mov byte [%1 + %2], MACRO_BYTE_REG
	sar MACRO_REG, 32
	min MACRO_WORD_REG, %[BYTE_MAX]w
	mov byte [%1 + %2 + PIX_SIZE], MACRO_BYTE_REG
	movhlps %3, %3

	cvtps2pi mm0, %3
	movq MACRO_REG, mm0
	min MACRO_WORD_REG, %[BYTE_MAX]w
	mov byte [%1 + %2 + PIX_SIZE * 2], MACRO_BYTE_REG
	sar MACRO_REG, 32
	min MACRO_WORD_REG, %[BYTE_MAX]w
	mov byte [%1 + %2 + PIX_SIZE * 3], MACRO_BYTE_REG
%endmacro

%macro calc_color 6
	movaps xmm0, %1
	mulps xmm0, [%4]

	movaps xmm1, %2
	mulps xmm1, [%4 + XMM_SIZE]
	addps xmm0, xmm1

	movaps xmm1, %3
	mulps xmm1, [%4 + XMM_SIZE * 2]
	addps xmm0, xmm1

	unfill_xmm %6, %5, xmm0
%endmacro

global calc_sepia_colors

section .text

; void calc_sepia_colors(struct pixel* pix, vec* matrix)
calc_sepia_colors:
	mov BYTE_MAX, 255
	fill_xmm rdi, 0, xmm5
	fill_xmm rdi, 1, xmm6
	fill_xmm rdi, 2, xmm7
	calc_color xmm5, xmm6, xmm7, rsi, 0, rdi
	calc_color xmm5, xmm6, xmm7, rsi + XMM_SIZE * 3, 1, rdi
	calc_color xmm5, xmm6, xmm7, rsi + XMM_SIZE * 6, 2, rdi
	ret
