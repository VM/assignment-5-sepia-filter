#pragma once
#ifndef _IMAGE_UTILS
#define _IMAGE_UTILS

#include <stdint.h>
#include <stdio.h>
#include <sys/types.h>

struct __attribute__((__packed__))  pixel {
	uint8_t b, g, r;
};

struct image {
  uint64_t width, height;
  struct pixel* data;
};

void update_image(struct image* img, uint64_t width, uint64_t height, struct pixel* const data);

void free_image(struct image* img);

void copy_image(struct image const img, struct image* dest);

struct pixel* new_pixels(size_t width, size_t height);

size_t calc_pixels_line_size(uint64_t width);

uint64_t get_width(struct image const img);
uint64_t get_height(struct image const img);

// Returns a pointer to the line of pixels or full pixel array if line == -1
struct pixel* get_pixels(struct image const img, ssize_t line);

struct pixel* get_pixel(struct image const img, uint64_t x, uint64_t y);

void set_pixel(struct image img, uint64_t x, uint64_t y, struct pixel const* pix);
#endif

