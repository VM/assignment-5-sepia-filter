#include "bmp_format.h"

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "image.h"


#define FILE_HEADER_BITS 0x4d42
#define BITS_PER_PIXEL 24

struct __attribute__((__packed__))  bmp_header {
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};

struct bmp_header new_bmp_header(size_t width, size_t height){
	struct bmp_header header;
	header.bfType = FILE_HEADER_BITS;
	header.bfileSize = sizeof(struct bmp_header) + 3 * width * height + calc_pixels_line_size(width) % 4 * height;
	header.bfReserved = 0;
	header.bOffBits = sizeof(struct bmp_header);// Pixels offset from file start
	header.biSize = sizeof(struct bmp_header) - sizeof(uint32_t) * 3 - sizeof(uint16_t);// Header size, but without core (4 fields higher)
	header.biWidth = width;
	header.biHeight = height;
	header.biPlanes = 1;
	header.biBitCount = BITS_PER_PIXEL;// Bits per pixel
	header.biCompression = 0;
	header.biSizeImage = header.bfileSize - sizeof(struct bmp_header);
	header.biXPelsPerMeter = 2834;
	header.biYPelsPerMeter = 2834;
	header.biClrUsed = 0;
	header.biClrImportant = 0;
	return header;
}

void print_bmp(struct bmp_header const* st);

enum read_status from_bmp( FILE* in, struct image* img ){
	struct bmp_header bmp_header = {0};
	if(fread(&bmp_header, sizeof(struct bmp_header), 1, in) > 0){
		if(bmp_header.bfType != FILE_HEADER_BITS)
			return READ_INVALID_SIGNATURE;

		if(bmp_header.biBitCount != BITS_PER_PIXEL)
			return READ_INVALID_BITS;
	}

	if(fseek(in, (long)(bmp_header.bOffBits - sizeof(struct bmp_header)), SEEK_CUR) != 0)
		return READ_INVALID_HEADER;

	update_image(img, (long)bmp_header.biWidth, bmp_header.biHeight, new_pixels(bmp_header.biWidth, bmp_header.biHeight));
	if (img->data == NULL)
	  return READ_ALLOCATION_ERROR;

	const size_t line_size = calc_pixels_line_size(bmp_header.biWidth);
	const uint8_t padding = line_size % 4 == 0 ? 0 : 4 - line_size % 4;

	for(size_t i = 0; i < bmp_header.biHeight; i++){

		if(fread((void*)get_pixels(*img, (ssize_t)i), line_size, 1, in) != 1){
			printf("line %d \n", (int)i);
			free_image(img);
			return READ_INVALID_PIXELS;
		}

		if(fseek(in, padding, SEEK_CUR) != 0){
			free_image(img);
			return READ_INVALID_PIXELS;
		}
	}
	return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ){
	struct bmp_header bmp_header = new_bmp_header(get_width(*img), get_height(*img));
	if(fwrite(&bmp_header, sizeof(struct bmp_header), 1, out) != 1)
		return WRITE_ERROR;

	const size_t line_size = calc_pixels_line_size(bmp_header.biWidth);
	const uint8_t padding = line_size % 4 == 0 ? 0 : 4 - line_size % 4;

	for(size_t i = 0; i < get_height(*img); i++){
		if(fwrite((void*)get_pixels(*img, (ssize_t)i), line_size, 1, out) != 1)
			return WRITE_ERROR;
		if(fseek(out, padding, SEEK_CUR) != 0)
			return WRITE_ERROR;
	}
	fwrite("\0", 1, 1, out);

	return WRITE_OK;
}

void print_bmp(struct bmp_header const* st)
{
    printf("Contents of structure bmp_header are");
    printf("\nHEADER: %" PRId16, st->bfType);
	printf("\nFILE SIZE:%" PRId32, st->bfileSize);
	printf("\nRESERVED: %" PRId32, st->bfReserved);
	printf("\nPIXELS OFFSET: %" PRId32, st->bOffBits);
	printf("\nHEADER SIZE: %" PRId32, st->biSize);
	printf("\nW: %" PRId32, st->biWidth);
	printf("\nH: %" PRId32, st->biHeight);
	printf("\nPLANES: %" PRId16, st->biPlanes);
	printf("\nBITS: %" PRId16, st->biBitCount);
	printf("\nCOMPRESSION: %" PRId32, st->biCompression);
	printf("\nSIZE BYTES: %" PRId32, st->biSizeImage);
	printf("\nX PIXELS PER METER: %" PRId32, st->biXPelsPerMeter);
	printf("\nY PIXELS PER METER: %" PRId32, st->biYPelsPerMeter);
	printf("\nUSED COLORS: %" PRId32, st->biClrUsed);
	printf("\nIMPORTANT COLORS: %" PRId32 "\n", st->biClrImportant);
}
